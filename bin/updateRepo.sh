#!/usr/bin/env bash

# If this command was called without any arguments, assume the caller doesn't
# know what they're doing, and provide usage information.
if [ "$1" != "--overwrite" ]
then
  echo "Usage: $0 --overwrite
Fetches code updates from the git origin remote and applies them to the local repository."
  exit 0
fi

git fetch origin

if [ $? -eq 128 ]
then
  echo "The current working directory is not a git repository with an origin remote." >&2
  exit 1
fi

# See if there is a difference between the working directory and upstream.
git diff origin/master --exit-code > /dev/null

if [ $? -eq 0 ]
then
  # There were no changes.
  echo "There are no code updates to apply." >&2
  exit 0
fi

# Apply upstream changes.
git reset --hard origin/master
echo "Local repository updated." >&2
touch ~/.restart_requested
exit 1
