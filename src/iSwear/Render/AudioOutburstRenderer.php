<?php

namespace iSwear\Render;

use iSwear\OutburstInterface;
use iSwear\OutburstRenderInterface;

/**
 * Responsible for rendering outbursts with an associated audio file.
 */
class AudioOutburstRenderer implements OutburstRenderInterface {

  /**
   * {@inheritdoc}
   */
  public function render(OutburstInterface $outburst) {
    $audio_file = $this->getAudioPath($outburst);
    $command = sprintf('ogg123 --quiet %s', escapeshellarg($audio_file));
    exec($command);
  }

  /**
   * Gets the path to the audio file.
   *
   * @param OutburstInterface $outburst
   *   The outburst containing the audio file path.
   *
   * @return string
   *   The path to the audio file.
   *
   * @throw \RuntimeException
   *   If the outburst media type is not audio or if the media data is missing.
   *
   * @throu \DomainException
   *   If the audio file cannot be found.
   */
  private function getAudioPath(OutburstInterface $outburst) {
    if ($outburst->getMediaType() !== OutburstInterface::MEDIA_TYPE_AUDIO) {
      throw new \RuntimeException('The media type is expected to be audio.');
    }
    $media_path = $outburst->getMediaData();
    if (empty($media_path)) {
      throw new \RuntimeException('The media data is not populated.');
    }
    $home = getenv('HOME');
    $result = sprintf('%s/iSwearMedia/audio/%s', $home, $media_path);
    if (!file_exists($result)) {
      throw new \DomainException(sprintf('The audio file "%s" cannot be found.', $result));
    }
    return $result;
  }
}
