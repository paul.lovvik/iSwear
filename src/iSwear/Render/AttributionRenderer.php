<?php

namespace iSwear\Render;

use iSwear\OutburstInterface;

/**
 * Responsible for indicating the owner of an outburst.
 */
class AttributionRenderer extends TtsOutburstRenderer {

  /**
   * Gets the text-to-speech string that will be rendered.
   *
   * @param OutburstInterface $outburst
   *   The outburst.
   *
   * @return string
   *   The outburst in a form suitable for text to speech software.
   */
  protected function getTtsText(OutburstInterface $outburst) {
    $result = NULL;
    $owner = $outburst->getOwnerTTS();
    if (empty($owner)) {
      // Fall back to using the text of the owner's name.
      $owner = $outburst->getOwner();
    }
    if (!empty($owner)) {
      return sprintf('%s says', $owner);
    }
  }
}
