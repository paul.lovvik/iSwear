<?php

namespace iSwear\Filter;

use iSwear\Outburst;
use iSwear\OutburstFilterInterface;
use iSwear\OutburstInterface;

/**
 * Filters outbursts based on the user's maximum degree preference.
 */
class OutburstDegreeFilter implements OutburstFilterInterface {

  /**
   * The user's tolerance for outbursts.
   *
   * @var int
   */
  private $maximumDegree = 100;

  /**
   * Configures this filter with the specified maximum degree.
   *
   * The maximum degree identifies the user's tolerance for outbursts.
   *
   * @param int $maximum_degree
   *   Optional. If provided outbursts will be regenerated as needed to comply
   *   with the user's tolerance.
   */
  public function __construct($maximum_degree = 100) {
    if (!is_int($maximum_degree) || $maximum_degree < 1 || $maximum_degree > 100) {
      throw new \InvalidArgumentException('The "maximum_degree" parameter must be a positive integer between 1 and 100.');
    }
    $this->maximumDegree = $maximum_degree;
  }

  /**
   * {@inheritdoc}
   */
  public function filter(OutburstInterface $outburst) {
    $result = $outburst;
    if ($outburst->getDegree() > $this->maximumDegree) {
      // The user prefers not to hear such messages. Take the message intent
      // and dial it back a notch. This is done by creating a sparse outburst
      // and allow the selection mechanism to do its job.
      $result = new Outburst();

      // Copy non-offensive details from the original outburst to provide
      // guidance when selecting a new outburst.
      $id = $outburst->getId();
      if (!empty($id)) {
        $result->setId($id);
      }

      // Set up to not exceed the user's tolerance.
      $result->setDegree(min($outburst->getDegree(), $this->maximumDegree));
      $owner = $outburst->getOwner();
      if (!empty($owner)) {
        $result->setOwner($owner);
      }
      $owner_tts = $outburst->getOwnerTTS();
      if (!empty($owner_tts)) {
        $result->setOwnerTTS($owner_tts);
      }
      $outburst_type = $outburst->getOutburstType();
      if (!empty($outburst_type)) {
        $result->setOutburstType($outburst_type);
      }
    } else {
      // The degree is not standing in our way. Let it fly!
    }
    return $result;
  }
}
