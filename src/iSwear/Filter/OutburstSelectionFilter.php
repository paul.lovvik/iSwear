<?php

namespace iSwear\Filter;

use iSwear\Outburst;
use iSwear\OutburstFilterInterface;
use iSwear\OutburstInterface;

/**
 * Responsible for selecting an appropriate outburst.
 */
class OutburstSelectionFilter implements OutburstFilterInterface {

  /**
   * The maximum degree to consider for the selection.
   *
   * @var int
   */
  private $outburstMaximumDegree = 0;

  /**
   * {@inheritdoc}
   */
  public function filter(OutburstInterface $outburst) {
    $result = $outburst;

    // Note: The specified outburst must contain the maximum degree. If the
    // user has defined a maximum degree, this outburst degree must not exceed
    // the user's preference. This must be done in a prior filter.
    $this->outburstMaximumDegree = $outburst->getDegree();

    if ($outburst->readyToRender($outburst)) {
      // The specified outburst is fine as is.
    } else {
      // The specified outburst is incomplete. Fill in the gaps by selecting an
      // appropriate outburst.
      $selected_outburst = $this->selectOutburst($outburst->getOutburstType());

      // Create a new outburst with the owner and ID of the provided outburst.
      // These fields must take priority over outburst that is selected from
      // the dictionary.
      $result = new Outburst();
      $id = $outburst->getId();
      if (!empty($id)) {
        $result->setId($id);
      }
      $owner = $outburst->getOwner();
      if (!empty($owner)) {
        $result->setOwner($owner);
      }
      $owner_tts = $outburst->getOwnerTTS();
      if (!empty($owner_tts)) {
        $result->setOwnerTTS($owner_tts);
      }

      // Copy the newly selected outburst onto the newly created outburst instance.
      $result->copyFromOutburst($selected_outburst);
    }

    return $result;
  }

  /**
   * Selects an appropriate outburst.
   *
   * @param int $outburst_type
   *   The type of outburst to choose from.
   *
   * @return OutburstInterface
   *   The newly selected outburst.
   */
  private function selectOutburst($outburst_type) {
    $outbursts = $this->getOutbursts($outburst_type);

    // The keys represent the degree for each set of outbursts.
    $outburst_degrees = array_keys($outbursts);
    sort($outburst_degrees);

    // Create a list of acceptable outburst degrees that actually exist in the
    // dictionary.
    $acceptable_outburst_degrees = array_filter($outburst_degrees, array($this, 'degreeFilter'));

    // Always choose the largest outburst degree.
    $selected_degree = end($acceptable_outburst_degrees);

    // Now select an actual outburst from the set.
    $possible_outbursts = $outbursts[$selected_degree];
    $result = $possible_outbursts[mt_rand(0, count($possible_outbursts) - 1)];
    return $result;
  }

  /**
   * @param int $outburst_type
   *   The type of outburst being requested.
   *
   * @return OutburstInterface[][]
   *   The set of outbursts of the specified type.
   */
  public function getOutbursts($outburst_type) {
    $all_outbursts = Outburst::getAllOutbursts();
    return $all_outbursts[$outburst_type];
  }

  /**
   * Indicates whether or not the specified degree is within the desired range.
   *
   * @param int $degree
   *   The degree.
   *
   * @return bool
   *   TRUE if the specified degree is within the desired range; FALSE otherwise.
   */
  public function degreeFilter($degree) {
    $result = TRUE;
    if ($degree > $this->getMaximumDegree()) {
      $result = FALSE;
    }
    return $result;
  }

  /**
   * Gets the maximum outburst degree this user is willing to tolerate.
   *
   * @return int
   *   The maximum outburst degree.
   */
  public function getMaximumDegree() {
    return $this->outburstMaximumDegree;
  }
}
