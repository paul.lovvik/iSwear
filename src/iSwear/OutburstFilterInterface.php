<?php

namespace iSwear;

/**
 * Describes all classes that filter or populate outburst instances.
 */
interface OutburstFilterInterface {

  /**
   * Filters the specified outburst.
   *
   * @param OutburstInterface $outburst
   *   The outburst instance to be filtered.
   *
   * @return OutburstInterface
   *   The filtered outburst.
   */
  public function filter(OutburstInterface $outburst);

}
