<?php

namespace iSwear;

use PDO;

/**
 * Responsible for storing and retrieving outbursts.
 */
class OutburstStorage {

  private $schemaVersion = 1;
  /**
   * Creates a new instance of MonitorDatabase.
   *
   * @param Config $config
   *   The application configuration.
   */
  public function __construct(Config $config = NULL) {
    if (NULL == $config) {
      $config = Config::create();
    }
    try {
      $dbConnect = sprintf(
        'mysql:host=%s;dbname=%s',
        $config->getDatabaseServer(),
        $config->getDatabaseName()
      );

      $this->db = new PDO(
        $dbConnect,
        $config->getDatabaseUser(),
        $config->getDatabasePassword()
      );
      if (!$this->tablesExist()) {
        $this->createTables();
      }
    } catch (\Exception $e) {
      die($e->getMessage() . "\n");
    }
  }

  /**
  private function needsSchemaUpdate() {
    $dbSchema = $this->db->query('SELECT MAX(schema_version) FROM metadata')->fetchColumn(0);
    return $dbSchema < $this->schemaVersion;
  }

   * Determines whether the database tables exist.
   *
   * @return bool
   *   TRUE if the database tables exist; FALSE otherwise.
   */
  private function tablesExist() {
    // If this throws an error the tables do not exist.
    try {
      if (FALSE !== $this->db->query('SELECT 1 FROM metadata LIMIT 1')) {
        return TRUE;
      }
    } catch (\Exception $e) {
    }
    return FALSE;
  }

  private function createTables() {
    // timestamp, exit_code, ping_time, external, address
    $this->db->exec("DROP TABLE outburst;");
    $pingTable = <<<EOT
CREATE TABLE IF NOT EXISTS outburst (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  timestamp INT UNSIGNED DEFAULT 0,
  owner varchar(256) DEFAULT NULL,
  owner_tts varchar(256) DEFAULT NULL,
  degree INT UNSIGNED DEFAULT 0,
  outburst_type INT UNSIGNED DEFAULT 0,
  media_type INT UNSIGNED DEFAULT 0,
  text varchar(256) DEFAULT NULL,
  media_data varchar(512) DEFAULT NULL,
  processed INT UNSIGNED DEFAULT 0,
  local INT UNSIGNED DEFAULT 0
)
EOT;
    $this->db->exec($pingTable);

    $metadataTable = <<<EOT
CREATE TABLE IF NOT EXISTS metadata (
  schema_version INT UNSIGNED DEFAULT 0
)
EOT;

    $this->db->exec("DROP TABLE metadata;");
    $this->db->exec($metadataTable);
    $this->db->exec('INSERT INTO metadata VALUES (' . $this->schemaVersion . ')');
  }


  /**
   * Saves the specified outburst.
   *
   * @param OutburstInterface $outburst
   *   The outburst.
   */
  public function saveOutburst(OutburstInterface $outburst) {
    $insert_template = <<<EOT
INSERT INTO outburst (timestamp, owner, owner_tts, degree, outburst_type, media_type, text, media_data, local) VALUES
(%s, %s, %s, %s, %s, %s, %s, %s, %s)
EOT;
    $time = $outburst->getTimestamp();
    if (empty($time)) {
      $time = time();
    }
    $db_insert = sprintf($insert_template,
      $this->db->quote($time),
      $this->db->quote($outburst->getOwner()),
      $this->db->quote($outburst->getOwnerTTS()),
      $this->db->quote($outburst->getDegree()),
      $this->db->quote($outburst->getOutburstType()),
      $this->db->quote($outburst->getMediaType()),
      $this->db->quote($outburst->getText()),
      $this->db->quote($outburst->getMediaData()),
      $this->db->quote((int) $outburst->getLocal())
    );
    $this->db->exec($db_insert);
  }

  /**
   * Gets the next outburst.
   *
   * @return OutburstInterface
   *   The next outburst, or NULL if there are no more outbursts to process.
   */
  public function getNextOutburst() {
    $result = NULL;
    $query = $this->db->query('SELECT * FROM outburst WHERE processed = 0 ORDER BY timestamp ASC LIMIT 1');
    if (FALSE !== $query) {
      $query_result = $query->fetchObject();
      if (empty($query_result)) {
        return $result;
      }
      $result = new Outburst();
      if (!empty($query_result->id)) {
        $result->setId(intval($query_result->id));
      }
      if (!empty($query_result->timestamp)) {
        $result->setTimestamp(intval($query_result->timestamp));
      }
      if (!empty($query_result->owner)) {
        $result->setOwner($query_result->owner);
      }
      if (!empty($query_result->owner_tts)) {
        $result->setOwnerTTS($query_result->owner_tts);
      }
      if (!empty($query_result->degree)) {
        $result->setDegree(intval($query_result->degree));
      }
      if (!empty($query_result->outburst_type)) {
        $result->setOutburstType(intval($query_result->outburst_type));
      }
      if (!empty($query_result->media_type)) {
        $result->setMediaType(intval($query_result->media_type));
      }
      if (!empty($query_result->text)) {
        $result->setText($query_result->text);
      }
      if (!empty($query_result->media_data)) {
        $result->setMediaData($query_result->media_data);
      }
      $result->setLocal(!empty($query_result->local));
    }
    return $result;
  }

  /**
   * Marks the specified outburst as having been processed.
   *
   * @param OutburstInterface $outburst
   *   The outburst to mark as processed.
   */
  public function markOutburstAsProcessed(OutburstInterface $outburst) {
    $update_template = <<<EOT
UPDATE outburst SET processed = 1 WHERE id = %s
EOT;
    $db_update = sprintf($update_template,
      $this->db->quote($outburst->getId())
    );
    $this->db->exec($db_update);
  }
}
