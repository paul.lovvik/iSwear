<?php

namespace iSwear;

use iSwear\Filter\MessageEnqueueOutburstFilter;
use iSwear\Filter\OutburstDegreeFilter;
use iSwear\Filter\OutburstDelayFilter;
use iSwear\Filter\OutburstSelectionFilter;
use iSwear\Filter\RemoveOwnerOutburstFilter;
use iSwear\Render\AttributionRenderer;
use iSwear\Render\AudioOutburstRenderer;
use iSwear\Render\TtsOutburstRenderer;

/**
 * This class is responsible for monitoring the iSwear queue and rendering.
 */
class iSwear {

  /**
   * The outburst storage instance.
   *
   * @var OutburstStorage
   */
  private $outburstStorage = NULL;

  /***
   * The configuration data for this iSwear.
   *
   * @var Config
   */
  protected $config;

  public function __construct() {
    $this->config = Config::create();
  }

  /**
   * The event loop.
   *
   * This method polls the database queue to find unprocessed outbursts and
   * processes them.
   */
  public function processLoop() {
    $outburst_storage = $this->getOutburstStorage();
    do {
      $outburst = $outburst_storage->getNextOutburst();
      if (!empty($outburst)) {
        /** @var OutburstFilterInterface[] $filters */
        $filters = $this->getOutburstFilters($outburst);
        foreach ($filters as $filter) {
          $outburst = $filter->filter($outburst);
        }

        // Render the outburst. Note that this could involve one or more
        // renderers; attribution is performed by a separate renderer.
         $renderers = $this->getRenderers($outburst);
        foreach ($renderers as $renderer) {
          $renderer->render($outburst);
        }
        $outburst_storage->markOutburstAsProcessed($outburst);
      }
      // Leave a little bit of space between outbursts.
      sleep(2);

    } while (!$this->restartRequested());
  }

  /**
   * Determines whether another process has requested a graceful restart.
   *
   * @return bool
   */
  public function restartRequested() {
    $flag = Config::getHomeDirectory() . '/.restart_requested';
    $restartRequested = file_exists($flag);
    if ($restartRequested) {
      // Now that we've received the signal to restart, we can clear it.
      unlink($flag);
    }
    return $restartRequested;
  }

  /**
   * Gets my owner name.
   *
   * This requires that the configuration file ~/config.ini be populated with
   * a 'name' field. If that field is missing 'Anonymous' will be returned
   * instead.
   *
   * @return string
   *   My owner name.
   */
  public function getMyName() {
    $my_name = $this->config->name;
    if (empty($my_name)) {
      $my_name = 'Anonymous';
    }
    return $my_name;
  }

  /**
   * Gets the set of outburst filters.
   *
   * @param OutburstInterface $outburst
   *   The outburst.
   *
   * @return OutburstFilterInterface[]
   *   The outburst filters.
   */
  public function getOutburstFilters(OutburstInterface $outburst) {
    $result = array();

    // Be sure to honor the user's configured degree tolerance.
    $result[] = new OutburstDegreeFilter($this->config->getUserMaximumDegree());

    // Be sure the outburst is complete; populate it if required.
    $result[] = new OutburstSelectionFilter();

    if ($this->isMyOutburst($outburst) && $outburst->getLocal() == 1) {
      // Send my outbursts to the message queue, with attribution.
      $result[] = new MessageEnqueueOutburstFilter($this->config);

      // Before rendering locally, get rid of my attribution so I don't hear it.
      $result[] = new RemoveOwnerOutburstFilter();
    }
    else if (!$this->isMyOutburst($outburst)) {
      $delay = $this->config->getRenderDelay();
      if (is_numeric($delay) && $delay > 0) {
        // The user configuration has a render delay.
        $result[] = new OutburstDelayFilter(floatval($delay));
      }
    }
    return $result;
  }

  /**
   * Gets the set of renderers for the specified outburst.
   *
   * @param OutburstInterface $outburst
   *   The outburst.
   *
   * @return OutburstRenderInterface[]
   *   The set of outburst renderers appropriate for the specified outburst.
   */
  public function getRenderers(OutburstInterface $outburst) {
    $result = array();
    $mine = $this->isMyOutburst($outburst);
    $local = $outburst->getLocal();

    if ($mine && !$local) {
      // This has just come back to us from the message queue. We do not want
      // to hear it again. Don't render anything.
      return $result;
    }
    if (!$mine) {
      // This belongs to someone else. Let's hear who it is.
      $result[] = new AttributionRenderer();
    }
    switch ($outburst->getMediaType()) {
      case OutburstInterface::MEDIA_TYPE_TTS:
      case OutburstInterface::MEDIA_TYPE_TTS_TEMPLATE:
        $result[] = new TtsOutburstRenderer();
        break;

      case OutburstInterface::MEDIA_TYPE_AUDIO:
        $result[] = new AudioOutburstRenderer();
        break;

      default:
        throw new \RuntimeException('Media type is not valid.');
    }
    return $result;
  }

  /**
   * Indicates whether the specified outburst is from me.
   *
   * Note that such an outburst should never actually be queued for processing
   * because it could set up an infinite swear loop.
   *
   * @param Outburstinterface $outburst
   *   The outburst instance.
   *
   * @return bool
   *   TRUE if the specified outburst is from me; FALSE otherwise.
   */
  public function isMyOutburst(Outburstinterface $outburst) {
    return $outburst->getOwner() === $this->getMyName();
  }

  /**
   * Gets the output storage instance.
   *
   * @return OutburstStorage
   *   The outburst storage instance.
   */
  private function getOutburstStorage() {
    if (empty($this->outburstStorage)) {
      $this->outburstStorage = new OutburstStorage();
    }
    return $this->outburstStorage;
  }
}
