weSwear
======

This is the application that allows the iSwear devices to share outbursts.

API
---

To send messages.
```php
<?php

require_once('vendor/autoload.php');

$client = new \weSwear\Broadcast("<wesweard host>", "5555");
$client->connect();
$client->write((string)time());
$client->close();

```

To receive messages
```php
<?php

require_once('vendor/autoload.php');

$client = new \weSwear\Receive("<wesweard host>", "4444");
$client->connect();
while (true) {
  echo "waiting...\n";
  $message = $client->read();
  echo "received $message\n";
}
$client->close();
```
