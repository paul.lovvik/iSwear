<?php

namespace weSwear;

class Client {

  private $host;

  private $port;

  private $socket;

  private $message_fragment = '';

  function __construct($host, $port) {
    if (empty($host) || empty($port)) {
      throw new Exception("Host and port are required to create a client.");
    }
    $this->host = $host;
    $this->port = $port;
  }

  function connect() {
    $this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    if ($this->socket === false) {
        throw new Exception("socket_create() failed: reason: " . socket_strerror(socket_last_error()));
    }

    $result = socket_connect($this->socket, $this->host, $this->port);
    if ($result === false) {
        throw new Exception("socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($this->socket)));
    }
  }

  function read() {
    $message = '';
    while ($out = socket_read($this->socket, 4096)) {
      $message = trim($out);
      // The service sends empty messages as heartbeats,
      // which we can safely ignore.
      if (!empty($message)) {
        break;
      }
    }

    return trim($message);
  }

  function write($message) {
    socket_write($this->socket, $message, strlen($message));
  }

  function close() {
    socket_close($this->socket);
  }

}
